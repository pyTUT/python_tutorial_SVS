#!/usr/bin/python
# -*- coding: utf-8 -*-

# dependcy
import matplotlib.pyplot as plt
import Tkinter
import os
import tkFileDialog
import tkMessageBox
from Tkinter import *
from sepVocal import main
from PIL import ImageTk, Image
import scipy.io.wavfile as wav
from pydub import AudioSegment
import SIMM
import scipy.io.wavfile as wav
import os
from tracking import viterbiTrackingArray
from pydub import AudioSegment
import numpy as np
import time, os
from numpy import arange, zeros, array, argmax, vstack, amax, ones, outer
from numpy.random import randn
from string import join

def trans_mp4(input_file):
    output = input_file.split('.')[0] + '.mp3'

    cmd = "ffmpeg -i " + input_file + " -y " + output
    print cmd
    os.system(cmd)

    return output

def trans_format_wav(input_Audio_File):
    format = input_Audio_File.split('.')[1]

    if format == 'wav':
        transformed_wav = input_Audio_File
    elif format == 'mp3':
        song = AudioSegment.from_mp3(input_Audio_File)
        out_wav = input_Audio_File.split('.')[0] + '.wav'
        song.export(out_wav, 'wav')
        transformed_wav = out_wav
    elif format == 'mp4':
        trans_mp3 = trans_mp4(input_Audio_File)
        song = AudioSegment.from_mp3(trans_mp3)
        out_wav = trans_mp3.split('.')[0] + '.wav'
        song.export(out_wav, 'wav')
        transformed_wav = out_wav

    return transformed_wav

# input_files='input.mp4'
# print input_files
# out_file=trans_mp4(input_files)
# print out_file
# out_wav=trans_format_wav(out_file)
# print out_file

# wav =trans_format_wav(trans_mp4(input_files))
# print wav
def file_path(FILE_PATH='/home/2.wav'):
    if os.path.isdir(FILE_PATH):
        print 'dir %s exists' % (FILE_PATH)
        pass
    else:
        print  'dir %s not exists' % (FILE_PATH)
        os.makedirs(FILE_PATH)
    return 0

def split_long_music_to_10_second_pieces(input_song='input.wav'):

    song=AudioSegment.from_wav(input_song)
    ss=10000
    print song.duration_seconds
    length_song=len(song)
    for part in range(length_song/ss):
        print part
        file_path('temp/')
        song[part*ss:ss*(part+1)].export('temp/'+input_song.split('.')[0]+'_'+str(part)+'.wav','wav')

    return 0

def combination_all_pieces(path_to_file='temp'):
    for parent, dirnames, filenames in os.walk(path_to_file):
            total_num=len(filenames)
    song = AudioSegment.from_wav("temp/input_0.wav")

    for part in range(total_num-1):
        song = song+AudioSegment.from_wav("temp/input_"+str(part+1)+'.wav')
    song.export('combination.wav','wav')
    return 0

# split_long_music_to_10_second_pieces('input.wav')
combination_all_pieces('temp')