# python_tutorial_SVS
# (S)singing (V)voice (S)separation

## Release Version

**[beta_1.0](http://www.github.com/beata_1.0)**

## index.py

## imageMatlab.py

This is more or less a wrapper for Matplotlib imaging functions such that their behavior is equivalent, in terms of colormap, aspect and so forth, to the expected behavior of Matlab's functions.   

## sepVocal.py

This script can be used to execute the desired separation. See below for an example of use of this file.

## SIMM.py

This script implements the actual algorithm for parameter  estimation. It is mainly used by sepVocal.py.

## tracking.py

The Viterbi decoding algorithm is implemented in this script.

## Requirements:
These scripts have been tested with Python 2.7, 
    
The packages that are required to run the scripts are pydub,ffmepg, Numpy, Spicy, Matplotlib. One can respectively find the latest versions at the following addresses:  

* http://pydub.com/
* https://ffmpeg.org   
* http://numpy.org/
* http://scipy.org/
* http://matplotlib.sourceforge.net/

### Notes:
Prefer recent versions of the above packages, in order to avoid compatibility issues, notably for Matplotlib. Note that this latter package is not necessary for the program to run, although you might want to watch a bit what is happening!  
Spicy should be version 0.8+, since we use its **io.wavefile** module to read the wave files. We once used the audio lab module, but it would seem that it is a bit more complicated to install (with the benefit
that many more file formats are allowed).

## Usage:
    
The easy way to use these scripts is to run the exec package of our release version: http://www.github.com/beata_1.0   
for more develop: you can run the **index.py** on pycharm directly. 

###note: the output files will create under you source wav file.
## ContactMe

Email:xlzhang14@fudan.edu.cn
