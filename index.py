# -*- coding:cp936 -*-
import Tkinter
import os
import tkFileDialog
import tkMessageBox
from Tkinter import *
from sepVocal import main
from PIL import ImageTk, Image
import scipy.io.wavfile as wav
from pydub import AudioSegment

def get_vocal_music_mono():

    m1=AudioSegment.from_wav('1.wav')
    m2=AudioSegment.from_wav('2.wav')
    m3=AudioSegment.from_wav('3.wav')
    m4=AudioSegment.from_wav('4.wav')
    new_m=m1+m2+m3+m4
    new_m.export('wavfiles/mir1k.wav','wav')

    fs, song = wav.read('wavfiles/mir1k.wav')

    left = song[:, 0]
    right = song[:, 1]
    mono = left + right

    wav.write('wavfiles/mir1k_music.wav', fs, left)
    wav.write('wavfiles/mir1k_vocal.wav', fs, right)
    wav.write('wavfiles/mir1k_mono.wav', fs, mono)
    mono=AudioSegment.from_wav('wavfiles/mir1k_mono.wav')
    mono.set_channels(2).export("wavfiles/mix.wav",'wav')

    return 0


def get_screen_size(window):
    return window.winfo_screenwidth(), window.winfo_screenheight()


def get_window_size(window):
    return window.winfo_reqwidth(), window.winfo_reqheight()


def center_window(root, width, height):
    screenwidth = root.winfo_screenwidth()
    screenheight = root.winfo_screenheight()
    size = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
    print(size)
    root.geometry(size)


class show:
    num_info_hash = {}
    char_info_hash = {}
    num_char = {}
    char_num = {}

    def get_parm(self):
        nbiter = self.contents_nbiter.get()

        windowSize = self.contents_windowSize.get()

        fourierSize = self.contents_fourierSize.get()

        hopsize = self.contents_hopsize.get()

        R = self.contents_R.get()

        parm = {"nbiter": nbiter, "windowSize": windowSize, "fourierSize": fourierSize, "hopsize": hopsize, "R": R}

        return parm

    def vocal_sepration(self):
        tkMessageBox.showinfo('start  vocal separation now ?', 'if ok, separating will done after a moment! ')
        wav_path = self.contents_path_to_single_wav.get()
        parm = self.get_parm()
        main(wav_path, parm)
        tkMessageBox.showinfo('single file vocal separation',
                              'You file has been separated! and the output files will be opened ...')
        return 0

    def batch_vocal_sepration(self):
        tkMessageBox.showinfo('start batch files for vocal separation now ?',
                              'if ok, separating will done after a moment! ')
        wav_dir = self.contents_path_to_batch_files.get()
        for parent, dirnames, filenames in os.walk(wav_dir):
            for filename in filenames:
                if '.wav' in filename:
                    wav_file = os.path.join(parent, filename)
                    parm = self.get_parm()
                    main(wav_file, parm)
        tkMessageBox.showinfo('batch file vocal separation',
                              'You file has been separated! and the output files will be opened ... ')
        return 0

    def chose_wav(self):
        filename = tkFileDialog.askopenfilename()
        self.contents_path_to_single_wav.set(filename)
        return 0

    def chose_files(self):
        files = tkFileDialog.askdirectory()
        self.contents_path_to_batch_files.set(files)
        return 0

    def __init__(self):
        self.root = Tk()
        # w=Canvas(self.root,width=200,height=200,bg='blue')
        # w.pack()
        self.root.title("歌声分离/SingingVoiceSeparation".decode('gbk').encode('utf8'))

        # self.root.geometry('470x320')
        center_window(self.root, 385, 275)
        # self.root.maxsize(385, 285)
        # self.root.minsize(385, 285)

        MyButton(self.root, 1)
        # self.frm = Frame(self.root)
        # Top


        self.frm = Frame(self.root)

        # middle
        self.frm_M = Frame(self.frm)

        Label(self.frm_M, text="可选参数设定".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack()

        separator = Frame(self.frm_M, height=2, bd=1, relief=SUNKEN)
        separator.pack(fill=X, padx=5, pady=5)

        self.frm_M_left = Frame(self.frm_M)
        self.frm_M_left.pack(side=LEFT)

        Label(self.frm_M_left, text="迭代次数:".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack(side=TOP)
        Label(self.frm_M_left, text="加窗大小:".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack(side=TOP)
        Label(self.frm_M_left, text="傅里叶大小:".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack(side=TOP)
        Label(self.frm_M_left, text="hopsize:".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack(side=TOP)
        Label(self.frm_M_left, text="R:".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack(side=TOP)

        self.frm_M_right = Frame(self.frm_M)
        self.frm_M_right.pack(side=RIGHT)

        self.entrythingy_nbiter = Entry(self.frm_M_right)
        self.entrythingy_nbiter.pack(side=TOP)
        self.contents_nbiter = StringVar()
        self.contents_nbiter.set("10")
        self.entrythingy_nbiter.config(textvariable=self.contents_nbiter)

        self.entrythingy_windowSize = Entry(self.frm_M_right)
        self.entrythingy_windowSize.pack(side=TOP)
        self.contents_windowSize = StringVar()
        self.contents_windowSize.set("0.04644")
        self.entrythingy_windowSize.config(textvariable=self.contents_windowSize)

        self.entrythingy_fourierSize = Entry(self.frm_M_right)
        self.entrythingy_fourierSize.pack(side=TOP)
        self.contents_fourierSize = StringVar()
        self.contents_fourierSize.set("2048")
        self.entrythingy_fourierSize.config(textvariable=self.contents_fourierSize)

        self.entrythingy_hopsize = Entry(self.frm_M_right)
        self.entrythingy_hopsize.pack(side=TOP)
        self.contents_hopsize = StringVar()
        self.contents_hopsize.set("0.0058")
        self.entrythingy_hopsize.config(textvariable=self.contents_hopsize)

        self.entrythingy_R = Entry(self.frm_M_right)
        self.entrythingy_R.pack(side=TOP)
        self.contents_R = StringVar()
        self.contents_R.set("40")
        self.entrythingy_R.config(textvariable=self.contents_R)

        self.frm_M.pack()
        self.frm.pack()

        self.frm_Down = Frame(self.frm)
        separator = Frame(self.frm_Down, height=2, bd=1, relief=SUNKEN)
        separator.pack(fill=X, padx=5, pady=5)

        self.frm_Down.pack()

        self.frm_Down_Top = Frame(self.frm_Down)

        Label(self.frm_Down_Top, text="单个wav:".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack(side=LEFT)

        self.entrythingy_path_to_single_wav = Entry(self.frm_Down_Top)
        self.entrythingy_path_to_single_wav.pack(side=LEFT)
        self.contents_path_to_single_wav = StringVar()
        self.contents_path_to_single_wav.set("")
        self.entrythingy_path_to_single_wav.config(textvariable=self.contents_path_to_single_wav)

        Button(self.frm_Down_Top, text="选择文件".decode('gbk').encode('utf-8'), command=self.chose_wav, width=6, height=1,
               font=('Arial', 10)).pack(side=LEFT)

        Button(self.frm_Down_Top, text="开始分离".decode('gbk').encode('utf-8'), command=self.vocal_sepration, width=6,
               height=1,
               font=('Arial', 10)).pack(side=LEFT)

        self.frm_Down_Top.pack()

        self.frm_Down_Down = Frame(self.frm_Down)

        Label(self.frm_Down_Down, text="批量目录:".decode('gbk').encode('utf8'), font=('Arial', 15),
              ).pack(side=LEFT)

        self.entrythingy_path_to_batch_files = Entry(self.frm_Down_Down)
        self.entrythingy_path_to_batch_files.pack(side=LEFT)
        self.contents_path_to_batch_files = StringVar()
        self.contents_path_to_batch_files.set("")
        self.entrythingy_path_to_batch_files.config(textvariable=self.contents_path_to_batch_files)

        Button(self.frm_Down_Down, text="选择目录".decode('gbk').encode('utf-8'), command=self.chose_files, width=6,
               height=1,
               font=('Arial', 10)).pack(side=LEFT)

        Button(self.frm_Down_Down, text="批量分离".decode('gbk').encode('utf-8'), command=self.batch_vocal_sepration,
               width=6,
               height=1,
               font=('Arial', 10)).pack(side=LEFT)

        self.frm_Down_Down.pack()


class MyDialog:  # 定义对话框类
    def __init__(self, root):  # 对话框初始化
        self.top = Tkinter.Toplevel(root)  # 生成Toplevel组件
        label = Tkinter.Label(self.top, text='Please Input')  # 生成标签组件
        label.pack()
        self.entry = Tkinter.Entry(self.top)  # 生成文本框组件
        self.entry.pack()
        self.entry.focus()  # 让文本框获得焦点
        button = Tkinter.Button(self.top, text='Ok',  # 生成按钮
                                command=self.Ok)  # 设置按钮事件处理函数
        button.pack()

    def Ok(self):  # 定义按钮事件处理函数
        self.input = self.entry.get()  # 获取文本框中内容，保存为input
        self.top.destroy()  # 销毁对话框

    def get(self):  # 返回在文本框输入的内容
        return self.input


class MyButton():  # 定义按钮类
    def __init__(self, root, type):  # 按钮初始化
        self.root = root  # 保存父窗口引用
        if type == 0:  # 根据类型创建不同按钮
            self.button = Tkinter.Button(root,
                                         text='Single',
                                         command=self.Single)  # 设置Create按钮的事件处理函数
        elif type == 1:
            self.button = Tkinter.Button(root,
                                         text='Quit',
                                         command=self.Quit)  # 设置Quit按钮的事件处理函数
        elif type == 2:
            self.button = Tkinter.Button(root,
                                         text='Batch',
                                         command=self.Batch)  # 设置Create按钮的事件处理函数
        self.button.pack(side=TOP)

    def Single(self):  # Single按钮的事件处理函数
        d = MyDialog(self.root)  # 生成对话框
        self.button.wait_window(d.top)  # 等待对话框结束
        tkMessageBox.showinfo('wait for a momoent', 'You input:\n' + d.get())  # 获取对话框中输入值，并输出

    def Batch(self):  # Batch按钮的事件处理函数
        d = MyDialog(self.root)  # 生成对话框
        self.button.wait_window(d.top)  # 等待对话框结束
        tkMessageBox.showinfo('Python', 'You input:\n' + d.get())  # 获取对话框中输入值，并输出

    def Quit(self):  # Quit按钮的事件处理函数
        self.root.quit()  # 退出主窗口


def main_of_me():
    show()

    mainloop()


if __name__ == "__main__":

    main_of_me()
